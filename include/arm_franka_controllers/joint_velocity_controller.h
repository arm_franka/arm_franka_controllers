// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#pragma once

#include <string>
#include <vector>

#include <controller_interface/multi_interface_controller.h>
#include <franka_hw/franka_state_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <hardware_interface/robot_hw.h>
#include <ros/node_handle.h>
#include <ros/time.h>
#include <Eigen/Dense>
#include <Eigen/Geometry>
//#include <realtime_tools/realtime_publisher.h>
#include <arm_franka_controllers/JointDesiredVelocity.h>

namespace arm_franka_controllers {

class JointVelocityController : public controller_interface::MultiInterfaceController<
                                           hardware_interface::VelocityJointInterface,
                                           franka_hw::FrankaStateInterface> {
 public:
  bool init(hardware_interface::RobotHW* robot_hw, ros::NodeHandle& node_handle) override;
  void update(const ros::Time&, const ros::Duration& period) override;
  void starting(const ros::Time&) override;
  void stopping(const ros::Time&) override;

 private:
  hardware_interface::VelocityJointInterface* velocity_joint_interface_;
  std::vector<hardware_interface::JointHandle> velocity_joint_handles_;
  ros::Duration elapsed_time_;

  //ros::Duration time_max(8.0);
  //double omega, delta_theta_container;
  Eigen::Matrix<float,7,1> djoint_vel_;
  float qdot_des_[7];
  ros::Subscriber sub_qdot_des_;
  //realtime_tools::RealtimePublisher<arm_franka_controllers::JointDesiredVelocity> pub_qdot_des_;
  ros::Publisher pub_qdot_des_ ;
  void veldes_callback(const arm_franka_controllers::JointDesiredVelocity::ConstPtr& msg);


};

}  // namespace franka_example_controllers
