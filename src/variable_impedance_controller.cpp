// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE

#include <pinocchio/fwd.hpp>
#include <pinocchio/algorithm/kinematics.hpp>
#include <pinocchio/multibody/model.hpp>
#include <pinocchio/multibody/data.hpp>
#include <pinocchio/parsers/sample-models.hpp>
#include <pinocchio/algorithm/joint-configuration.hpp>
#include <pinocchio/algorithm/jacobian.hpp>
#include <pinocchio/algorithm/frames.hpp>
#include <pinocchio/algorithm/rnea.hpp>
#include <pinocchio/algorithm/aba.hpp>
#include <pinocchio/algorithm/crba.hpp>
#include <pinocchio/parsers/urdf.hpp>
#include <pinocchio/spatial/force.hpp>
#include <pinocchio/spatial/se3.hpp>
#include <pinocchio/math/rpy.hpp>

#include <arm_franka_controllers/variable_impedance_controller.h>

#include <cmath>
#include <memory>

#include <controller_interface/controller_base.h>
#include <franka/robot_state.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>


#include <arm_franka_controllers/pseudo_inversion.h>

namespace arm_franka_controllers {

bool VariableImpedanceController::init(hardware_interface::RobotHW* robot_hw,
                                               ros::NodeHandle& node_handle) {
  //std::vector<double> cartesian_stiffness_vector;
  //std::vector<double> cartesian_damping_vector;

  sub_equilibrium_pose_ = node_handle.subscribe(
      "equilibrium_pose", 1, &VariableImpedanceController::equilibriumPoseCallback, this,
      ros::TransportHints().reliable().tcpNoDelay());

  std::string arm_id;
  if (!node_handle.getParam("arm_id", arm_id)) {
    ROS_ERROR_STREAM("VariableImpedanceController: Could not read parameter arm_id");
    return false;
  }
  std::vector<std::string> joint_names;
  if (!node_handle.getParam("joint_names", joint_names) || joint_names.size() != 7) {
    ROS_ERROR(
        "VariableImpedanceController: Invalid or no joint_names parameters provided, "
        "aborting controller init!");
    return false;
  }

   // Initialize stiffness and damping gains
  Kd.setIdentity();
  Dd.setIdentity();
  Md.setIdentity();

  std::vector<double> Dd_damping;
  if (!node_handle.getParam("Dd_damping", Dd_damping) || Dd_damping.size() != 6) {
    ROS_ERROR(
      "VariableImpedanceController: Invalid or no Dd_damping parameters provided, "
      "aborting controller init!");
    return false;
  }
  std::vector<double> Kd_stiffness;
  if (!node_handle.getParam("Kd_stiffness", Kd_stiffness) || Kd_stiffness.size() != 6) {
    ROS_ERROR(
      "VariableImpedanceController: Invalid or no Kd_stiffness parameters provided, "
      "aborting controller init!");
    return false;
  }

  std::vector<double> Md_mass;
  if (!node_handle.getParam("Md_mass", Md_mass) || Md_mass.size() != 6) {
    ROS_ERROR(
      "VariableImpedanceController: Invalid or no Md_mass parameters provided, "
      "aborting controller init!");
    return false;
  }

for (int i = 0; i < 6; i ++) {
    Md(i,i) = Md_mass[i];
    Dd(i,i) = Dd_damping[i];
    Kd(i,i) = Kd_stiffness[i];
  }

 auto* model_interface = robot_hw->get<franka_hw::FrankaModelInterface>();
  if (model_interface == nullptr) {
    ROS_ERROR_STREAM(
        "ImpedanceExampleController: Error getting model interface from hardware");
    return false;
  }
  try {
    model_handle_ = std::make_unique<franka_hw::FrankaModelHandle>(
        model_interface->getHandle(arm_id + "_model"));
  } catch (hardware_interface::HardwareInterfaceException& ex) {
    ROS_ERROR_STREAM(
        "BRImpedanceController: Exception getting model handle from interface: "
        << ex.what());
    return false;
  }  
// This helps me to get the states of the robot 
  auto* state_interface = robot_hw->get<franka_hw::FrankaStateInterface>();
  if (state_interface == nullptr) {
    ROS_ERROR_STREAM(
        "VariableImpedanceController: Error getting state interface from hardware");
    return false;
  }
  try {
    state_handle_ = std::make_unique<franka_hw::FrankaStateHandle>(
        state_interface->getHandle(arm_id + "_robot"));
  } catch (hardware_interface::HardwareInterfaceException& ex) {
    ROS_ERROR_STREAM(
        "VariableImpedanceController: Exception getting state handle from interface: "
        << ex.what());
    return false;
  }
//********************************************************

  //
  auto* velocity_joint_interface_ = robot_hw->get<hardware_interface::VelocityJointInterface>();
  if (velocity_joint_interface_ == nullptr) {
    ROS_ERROR_STREAM(
        "VariableImpedanceController: Error getting effort joint interface from hardware");
    return false;
  }
  for (size_t i = 0; i < 7; ++i) {
    try {
      velocity_joint_handles_.push_back(velocity_joint_interface_->getHandle(joint_names[i]));
    } catch (const hardware_interface::HardwareInterfaceException& ex) {
      ROS_ERROR_STREAM(
          "VariableImpedanceController: Exception getting joint handles: " << ex.what());
      return false;
    }
  }

// This used for calling the parameteres of the desired interaction model
  dynamic_reconfigure_compliance_param_node_ =
      ros::NodeHandle(node_handle.getNamespace() + "/dynamic_reconfigure_compliance_param_node");

  dynamic_server_compliance_param_ = std::make_unique<
      dynamic_reconfigure::Server<arm_franka_controllers::compliance_paramConfig>>(

      dynamic_reconfigure_compliance_param_node_);
  dynamic_server_compliance_param_->setCallback(
      boost::bind(&VariableImpedanceController::complianceParamCallback, this, _1, _2));
// ******************************************************************************
  position_d_.setZero();
  orientation_d_.coeffs() << 0.0, 0.0, 0.0, 1.0;
  position_d_target_.setZero();
  orientation_d_target_.coeffs() << 0.0, 0.0, 0.0, 1.0;
  return true;
}

void VariableImpedanceController::starting(const ros::Time& /*time*/) {
  // compute initial velocity with jacobian and set x_attractor and q_d_nullspace
  // to initial configuration
  franka::RobotState initial_state = state_handle_->getRobotState();
  Eigen::Map<Eigen::Matrix<double, 7, 1>> q_initial(initial_state.q.data());
  Eigen::Affine3d initial_transform(Eigen::Matrix4d::Map(initial_state.O_T_EE.data()));

  // set equilibrium point to current state
  position_d_ = initial_transform.translation();
  orientation_d_ = Eigen::Quaterniond(initial_transform.rotation());
  position_d_target_ = initial_transform.translation();
  orientation_d_target_ = Eigen::Quaterniond(initial_transform.rotation());

  // set nullspace equilibrium configuration to initial q
  //q_d_nullspace_ = q_initial;

  const std::string urdf_filename = "/panda_ws/src/franka_description/robots/panda" + std::string("/panda.urdf");
  pinocchio::urdf::buildModel(urdf_filename,model_);
  data_     = pinocchio::Data(model_);
  frameIndex =  model_.getFrameId("panda_hand_tcp"); 
  Jp.setZero();
  xdot_task_ = Eigen::VectorXd(7);
  xdot_task_ <<0,0,0,0,0,0,0;
}

void VariableImpedanceController::update(const ros::Time& /*time*/,
                                                 const ros::Duration& /*period*/) {
  // get state variables
  franka::RobotState robot_state = state_handle_->getRobotState();
  Eigen::Map<Eigen::Matrix<double, 7, 1>> q(robot_state.q.data());
  Eigen::Map<Eigen::Matrix<double, 7, 1>> dq(robot_state.dq.data());
  Eigen::Map<Eigen::Matrix<double, 7, 1>> tau_J_d(  // NOLINT (readability-identifier-naming)
      robot_state.tau_J_d.data());
  Eigen::Affine3d transform(Eigen::Matrix4d::Map(robot_state.O_T_EE.data()));
  Eigen::Vector3d position(transform.translation());
  Eigen::Quaterniond orientation(transform.rotation());
  //Eigen::Map<Eigen::Matrix<double, 6, 1>> F_ext(robot_state.K_F_ext_hat_K.data());

  // Getting Jacobian from pinocchio
  pinocchio::forwardKinematics(model_,data_,q);
  pinocchio::updateFramePlacements(model_,data_);
  pinocchio::computeJointJacobians(model_,data_, q);
  pinocchio::getFrameJacobian(model_,data_,frameIndex, pinocchio::LOCAL_WORLD_ALIGNED, Jp);

  // compute error to desired pose
  // position error
  Eigen::Matrix<double, 6, 1> error;
  error.head(3) << position - position_d_;

  // orientation error
  if (orientation_d_.coeffs().dot(orientation.coeffs()) < 0.0) {
    orientation.coeffs() << -orientation.coeffs();
  }
  // "difference" quaternion
  Eigen::Quaterniond error_quaternion(orientation.inverse() * orientation_d_);
  error.tail(3) << error_quaternion.x(), error_quaternion.y(), error_quaternion.z();
  // Transform to base frame
  error.tail(3) << -transform.rotation() * error.tail(3);

  // compute control
  // allocate variables
  Eigen::VectorXd xbd_task(7), qdot_des(7);

  // pseudoinverse for nullspace handling
  // kinematic pseuoinverse
  Eigen::MatrixXd jacobian_transpose_pinv;
  pseudoInverse(Jp.transpose(), jacobian_transpose_pinv);


  xbd_task = Md.inverse()*(-Kd*error- Dd*(Jp*dq) );
  xdot_task_+= xbd_task*0.001;
  qdot_des = Jp.transpose()* xdot_task_;

  //std::cout << "Kd \n" << Kd << std::endl; 
  //std::cout << "Dd \n" << Dd << std::endl;  
  //std::cout << "xbd_task \n" << xdot_task_ << std::endl;

  
  for (size_t i = 0; i < 7; ++i) {
    velocity_joint_handles_[i].setCommand(qdot_des(i));
  }

  // // update parameters changed online either through dynamic reconfigure or through the interactive
  // // target by filtering
  // cartesian_stiffness_ =
  //     filter_params_ * cartesian_stiffness_target_ + (1.0 - filter_params_) * cartesian_stiffness_;
  // cartesian_damping_ =
  //     filter_params_ * cartesian_damping_target_ + (1.0 - filter_params_) * cartesian_damping_;
  // nullspace_stiffness_ =
  //     filter_params_ * nullspace_stiffness_target_ + (1.0 - filter_params_) * nullspace_stiffness_;
  std::lock_guard<std::mutex> position_d_target_mutex_lock(position_and_orientation_d_target_mutex_);
  position_d_ = filter_params_ * position_d_target_ + (1.0 - filter_params_) * position_d_;
  orientation_d_ = orientation_d_.slerp(filter_params_, orientation_d_target_);
}

void VariableImpedanceController::complianceParamCallback(
    arm_franka_controllers::compliance_paramConfig& config,
    uint32_t /*level*/) {
  cartesian_stiffness_target_.setIdentity();
  cartesian_stiffness_target_.topLeftCorner(3, 3)
      << config.translational_stiffness * Eigen::Matrix3d::Identity();
  cartesian_stiffness_target_.bottomRightCorner(3, 3)
      << config.rotational_stiffness * Eigen::Matrix3d::Identity();
  cartesian_damping_target_.setIdentity();
  // Damping ratio = 1
  cartesian_damping_target_.topLeftCorner(3, 3)
      << 2.0 * sqrt(config.translational_stiffness) * Eigen::Matrix3d::Identity();
  cartesian_damping_target_.bottomRightCorner(3, 3)
      << 2.0 * sqrt(config.rotational_stiffness) * Eigen::Matrix3d::Identity();
  nullspace_stiffness_target_ = config.nullspace_stiffness;
}

void VariableImpedanceController::equilibriumPoseCallback(
    const geometry_msgs::PoseStampedConstPtr& msg) {
  std::lock_guard<std::mutex> position_d_target_mutex_lock(
      position_and_orientation_d_target_mutex_);
  position_d_target_ << msg->pose.position.x, msg->pose.position.y, msg->pose.position.z;
  Eigen::Quaterniond last_orientation_d_target(orientation_d_target_);
  orientation_d_target_.coeffs() << msg->pose.orientation.x, msg->pose.orientation.y,
      msg->pose.orientation.z, msg->pose.orientation.w;
  if (last_orientation_d_target.coeffs().dot(orientation_d_target_.coeffs()) < 0.0) {
    orientation_d_target_.coeffs() << -orientation_d_target_.coeffs();
  }
}


}  // namespace arm_franka_controllers

PLUGINLIB_EXPORT_CLASS(arm_franka_controllers::VariableImpedanceController,
                       controller_interface::ControllerBase)
