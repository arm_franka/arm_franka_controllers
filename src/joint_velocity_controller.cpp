// Copyright (c) 2017 Franka Emika GmbH
// Use of this source code is governed by the Apache-2.0 license, see LICENSE
#include <arm_franka_controllers/joint_velocity_controller.h>

#include <cmath>

#include <controller_interface/controller_base.h>
#include <hardware_interface/hardware_interface.h>
#include <hardware_interface/joint_command_interface.h>
#include <pluginlib/class_list_macros.h>
#include <ros/ros.h>
#include <arm_franka_controllers/JointDesiredVelocity.h>

namespace arm_franka_controllers {

bool JointVelocityController::init(hardware_interface::RobotHW* robot_hw,
                                          ros::NodeHandle& node_handle) {

  pub_qdot_des_ = node_handle.advertise<arm_franka_controllers::JointDesiredVelocity>("out/qdot_des",1);
  //pub_qdot_des_.init(node_handle, "qdot_des", 1);

  sub_qdot_des_ = node_handle.subscribe("in/qdot_des", 1, &JointVelocityController::veldes_callback, this, ros::TransportHints().reliable().tcpNoDelay());
  // 
  std::string arm_id;
  if (!node_handle.getParam("arm_id", arm_id)) {
    ROS_ERROR("JointVelocityController: Could not get parameter arm_id");
    return false;
  }

  std::vector<std::string> joint_names;
  if (!node_handle.getParam("joint_names", joint_names) || joint_names.size() != 7) {
    ROS_ERROR(
        "VariableImpedanceController: Invalid or no joint_names parameters provided, "
        "aborting controller init!");
    return false;
  }

  velocity_joint_interface_ = robot_hw->get<hardware_interface::VelocityJointInterface>();
  if (velocity_joint_interface_ == nullptr) {
    ROS_ERROR(
        "JointVelocityController: Error getting velocity joint interface from hardware!");
    return false;
  }
  for (size_t i = 0; i < 7; ++i) {
    try {
      velocity_joint_handles_.push_back(velocity_joint_interface_->getHandle(joint_names[i]));
    } catch (const hardware_interface::HardwareInterfaceException& ex) {
      ROS_ERROR_STREAM(
          "VariableImpedanceController: Exception getting joint handles: " << ex.what());
      return false;
    }
  }
  
  // velocity_joint_handles_.resize(7);
  // for (size_t i = 0; i < 7; ++i) {
  //   try {
  //     velocity_joint_handles_[i] = velocity_joint_interface_->getHandle(joint_names[i]);
  //   } catch (const hardware_interface::HardwareInterfaceException& ex) {
  //     ROS_ERROR_STREAM(
  //         "JointVelocityController: Exception getting joint handles: " << ex.what());
  //     return false;
  //   }
  // }

  auto state_interface = robot_hw->get<franka_hw::FrankaStateInterface>();
  if (state_interface == nullptr) {
    ROS_ERROR("JointVelocityController: Could not get state interface from hardware");
    return false;
  }

  try {
    auto state_handle = state_interface->getHandle(arm_id + "_robot");

    std::array<double, 7> q_start{{0, -M_PI_4, 0, -3 * M_PI_4, 0, M_PI_2, M_PI_4}};
    for (size_t i = 0; i < q_start.size(); i++) {
      if (std::abs(state_handle.getRobotState().q_d[i] - q_start[i]) > 0.1) {
        ROS_ERROR_STREAM(
            "JointVelocityController: Robot is not in the expected starting position for "
            "running this example. Run `roslaunch franka_example_controllers move_to_start.launch "
            "robot_ip:=<robot-ip> load_gripper:=<has-attached-gripper>` first.");
        return false;
      }
    }
  } catch (const hardware_interface::HardwareInterfaceException& e) {
    ROS_ERROR_STREAM(
        "JointVelocityController: Exception getting state handle: " << e.what());
    return false;
  }

  return true;
}

void JointVelocityController::starting(const ros::Time& /* time */) {
  elapsed_time_ = ros::Duration(0.0);
}

void JointVelocityController::update(const ros::Time& /* time */,
                                            const ros::Duration& period) {
  //elapsed_time_ += period;
  arm_franka_controllers::JointDesiredVelocity desired_velocity;
  //ros::Duration time_max(8.0);
  // double omega_max = 0.1;
  // double cycle = std::floor(
  //     std::pow(-1.0, (elapsed_time_.toSec() - std::fmod(elapsed_time_.toSec(), time_max.toSec())) /
  //                        time_max.toSec()));
  // double omega = cycle * omega_max / 2.0 *
  //                (1.0 - std::cos(2.0 * M_PI / time_max.toSec() * elapsed_time_.toSec()));

  // for (auto joint_handle : velocity_joint_handles_) {
  //   joint_handle.setCommand(omega);
  // }
// if (elapsed_time_.toSec() < time_max) {
//   for (int j = 0; j < 7; j++) {
//   omega[j] = delta_theta_container[j];//omega_max[j] * std::sin(M_PI / time_max * elapsed_time_.toSec());
//   }
// }
// else {
//   for (int j = 0; j < 7; j++) {
//   omega[j] = 0.00000001;
//   }
// }
  double omega[7];
  desired_velocity.joint1 = djoint_vel_(0);
  desired_velocity.joint2 = djoint_vel_(1);
  desired_velocity.joint3 = djoint_vel_(2);
  desired_velocity.joint4 = djoint_vel_(3);
  desired_velocity.joint5 = djoint_vel_(4);
  desired_velocity.joint6 = djoint_vel_(5);
  desired_velocity.joint7 = djoint_vel_(6);
  pub_qdot_des_.publish(desired_velocity);
  // for (int j = 0; j < 7; j++) {
  //   omega[j] = ; 
  //   //desired_velocity.qdot_des = omega[j];
  // }
  //omega[6] = 0.1;
  // for (auto joint_handle : velocity_joint_handles_) {
  //   joint_handle.setCommand(omega);
  // }
  for (int j = 0; j < 7; j++) {
  velocity_joint_handles_[j].setCommand(djoint_vel_(j));
  }
}

void JointVelocityController::veldes_callback(const arm_franka_controllers::JointDesiredVelocity::ConstPtr& msg) {
  djoint_vel_<< (msg->joint1*M_PI)/180, (msg->joint2)*M_PI/180,(msg->joint3)*M_PI/180,
                (msg->joint4)*M_PI/180, (msg->joint5)*M_PI/180, (msg->joint6)*M_PI/180, (msg->joint7)*M_PI/180;
 //ROS_INFO("I heard:");
}


void JointVelocityController::stopping(const ros::Time& /*time*/) {
  // WARNING: DO NOT SEND ZERO VELOCITIES HERE AS IN CASE OF ABORTING DURING MOTION
  // A JUMP TO ZERO WILL BE COMMANDED PUTTING HIGH LOADS ON THE ROBOT. LET THE DEFAULT
  // BUILT-IN STOPPING BEHAVIOR SLOW DOWN THE ROBOT.
}

}  // namespace franka_example_controllers

PLUGINLIB_EXPORT_CLASS(arm_franka_controllers::JointVelocityController,
                       controller_interface::ControllerBase)